<?php


/**
 * @file
 * Functions to handle the Backup and Migrate Server backup destination.
 */

/**
 * A destination for sending database backups to an Backup and Migrate server.
 *
 * @ingroup backup_migrate_destinations
 */
class backup_migrate_destination_bams extends backup_migrate_destination_remote {
  var $supported_ops = array('scheduled backup', 'manual backup', 'restore', 'list files', 'configure', 'delete');

  /**
   * Save to the bam destination.
   */
  function save_file($file, $settings) {
    if ($destination = $this->_get_destination()) {
      srand((double)microtime()*1000000);
      $boundary = '---------------------------'. substr(md5(rand(0,32000)),0,10);


      $filename = $file->filename();
      $filesize = filesize($file->filepath());
      $ticket = $this->_xmlrpc('backups.getUploadTicket', $destination, $filename, $filesize);

      if ($ticket) {
        $data = '';

        foreach ((array)$ticket['params'] as $key => $value) {
          $data .="--$boundary\r\n";
          $data .= "Content-Disposition: form-data; name=\"".$key."\"\r\n";
          $data .= "\r\n".$value."\r\n";
          $data .="--$boundary\r\n";
        }
  
        // Add the file to the post payload.
        $data .="--$boundary\r\n";
        $data .= "Content-Disposition: form-data; name=\"file\"; filename=\"". $file->filename() ."\"\r\n";
        $data .= "Content-Type: application/octet-stream;\r\n";
        $data .= "\r\n".(file_get_contents($file->filepath()))."\r\n";
        $data .="--$boundary--\r\n";
    
        $headers = array(
          'Content-type' => "multipart/form-data, boundary=$boundary",
          'User-Agent' => 'Backup and Migrate',
        );
  
        $url = $ticket['url'];
  
        // If the ticket requires authentication add our username/password to the url.
        if (!empty($ticket['auth']) && $ticket['auth'] = 'basic') {
          $parts = parse_url($ticket['url']);
          $parts['user'] = @$this->dest_url['user'];
          $parts['pass'] = @$this->dest_url['pass'];
          $url = $this->glue_url($parts, FALSE);
        }

        $out = drupal_http_request($url, $headers, 'POST', $data);

        if ($out->code == 200) {
          // Confirm the upload.
          $confirm = $this->_xmlrpc('backups.confirmUpload', $destination, $filename, $filesize);

          if ($confirm['success']) {
            return $file;
          }
        }
        else {
          $error = !empty($out->headers['x-bams-error']) ? $out->headers['x-bams-error'] : $out->error;
          _backup_migrate_message('The server returned the following error: %err', array('%err' => $error), 'error');
        }
      }
      else if (!xmlrpc_error()) {
        _backup_migrate_message('The server refused the backup but did not specify why.');
      }
    }
    return NULL;
  }

  /**
   * Load from the bam destination.
   */
  function load_file($file_id) {
    if ($destination = $this->_get_destination()) {
      backup_migrate_include('files');
      $file = new backup_file(array('filename' => $file_id));

      $ticket = $this->_xmlrpc('backups.getDownloadTicket', $destination, $file_id);

      if ($ticket && $url = $ticket['url']) {
        // If the ticket requires authentication add our username/password to the url.
        if (!empty($ticket['auth']) && $ticket['auth'] = 'basic') {
          $parts = parse_url($ticket['url']);
          $parts['user'] = @$this->dest_url['user'];
          $parts['pass'] = @$this->dest_url['pass'];
          $url = $this->glue_url($parts, FALSE);
        }
  
        $out = drupal_http_request($url);

        if ($out->code == 200) {
          file_put_contents($file->filepath(), $out->data);
          return $file;
        }
        else {
          $error = !empty($out->headers['x-bams-error']) ? $out->headers['x-bams-error'] : $out->error;
          _backup_migrate_message('The server returned the following error: %err', array('%err' => $error), 'error');
        }
      }
    }
    return NULL;
  }

  /**
   * Delete from the bam destination.
   */
  function delete_file($file_id) {
    if ($destination = $this->_get_destination()) {
      $result = $this->_xmlrpc('backups.deleteFile', $destination, $file_id);
    }
  }

  /**
   * List the files in the remote destination.
   */
  function list_files() {
    $files = array();
    backup_migrate_include('files');

    if ($destination = $this->_get_destination()) {
      $file_list = $this->_xmlrpc('backups.listFiles', $destination);
  
      foreach ((array)$file_list as $file) {
        $files[$file['filename']] = new backup_file($file);
      }
    }

    return $files;
  }

  /**
   * List the files in the remote destination.
   */
  function list_destinations() {
    backup_migrate_include('files');

    $list = $this->_xmlrpc('backups.listDestinations');

    return $list;
  }

  /**
   * Get the form for the settings for this destination.
   */
  function edit_form() {
    $form = parent::edit_form();
    $form['scheme']['#type'] = 'value';
    $form['scheme']['#value'] = '';

    $location = '';
    if ($this->location) {
      $parts = parse_url($this->location);
      $parts['user'] = $form_state['values']['user'];
      $parts['pass'] = $form_state['values']['pass'];
      $location = $this->glue_url($parts, FALSE);
    }

    $form['location'] = array(
      "#type" => "textfield",
      "#title" => t("Server URL"),
      "#default_value" => $location,
      "#required" => TRUE,
      "#weight" => 10,
    );

    $options = array();

    if (!empty($this->dest_url['host']) && $this->dest_url['user'] && $this->dest_url['pass']) {
      $destinations = $this->list_destinations();
      if ($destinations) {
        if (!$this->settings('destination')) {
          drupal_set_message(t('Please pick a remote destination to use.'));
        }
        foreach ($destinations as $destination) {
          $options[$destination['id']] = $destination['name'];
        }
        $form['settings']['#tree'] = TRUE;
        $form['settings']['#weight'] = 50;
        $form['settings']['destination'] = array(
          "#type" => "select",
          "#title" => t("Remote Destination"),
          "#default_value" => $this->settings('destination'),
          '#options' => $options,
          "#weight" => 30,
        );
      }
    }
    else {
      $form['actions']['submit']['#value'] = t('Next Step »');
    }

    unset($form['path']);
    unset($form['host']);

    return $form;
  }

  /**
   * Submit the configuration form. Glue the url together and add the old password back if a new one was not specified.
   */
  function edit_form_validate($form, &$form_state) {
    if ($form_state['values']['pass']) {
      $parts = parse_url($form_state['values']['location']);
      $parts['user'] = $form_state['values']['user'];
      $parts['pass'] = $form_state['values']['pass'];
      $this->set_location($this->glue_url($parts, FALSE));
      $destinations = $this->list_destinations();

      if (!$destinations) {
        $err = xmlrpc_error();
        if ($err->code == '401') {
          form_set_error('user', 'Could not login to server. Please check that your username and password are correct.');
        }
        else {
          form_set_error('', 'The server did not list any available backup destinations. Please check your permissions on the backup server.');
        }
      }
    }
  }

  /**
   * Submit the configuration form. Glue the url together and add the old password back if a new one was not specified.
   */
  function edit_form_submit($form, &$form_state) {
    $form_state['values']['pass'] = $form_state['values']['pass'] ? $form_state['values']['pass'] : $form_state['values']['old_password'];

    // Merge the username/password back into the url.
    $parts = parse_url($form_state['values']['location']);
    $parts['user'] = $form_state['values']['user'];
    $parts['pass'] = $form_state['values']['pass'];

    $form_state['values']['location'] = $this->glue_url($parts, FALSE);
    $destinations = $this->list_destinations();
    // The user must select a destination.
    if (empty($form_state['values']['settings']['destination'])) {
      $this->from_array($form_state['values']);
      $form_state['rebuild'] = TRUE;
      return;
    }
    else {
      backup_migrate_destination::edit_form_submit($form, $form_state);
    }
  }

  /**
   * Get the destination id or warn the user that it has not been set.
   */
  function _get_destination($warn = TRUE) {
    if ($destination = $this->settings('destination')) {
      return $destination;
    }
    else if ($warn) {
      $edit = user_access('administer backup and migrate') ? l(t('Edit the destination'), 'admin/content/backup_migrate/destination/list/edit/'. $this->get_id()) : '';
      _backup_migrate_message('You must specify a remote destination. !edit', array('!edit' => $edit), 'error');
    }
  }

  /**
   * Glue a URL without encoding the username/password.
   */
  function glue_url($parts, $hide_password = TRUE) {
    // Obscure the password if we need to.
    $parts['pass'] = $hide_password ? "" : $parts['pass'];

    // Assemble the URL.
    $out = "";
    $out .= $parts['scheme'] .'://';
    $out .= $parts['user'] ? $parts['user'] : '';
    $out .= ($parts['user'] && $parts['pass']) ? ":". $parts['pass'] : '';
    $out .= ($parts['user'] || $parts['pass']) ? "@" : "";
    $out .= $parts['host'];
    $out .= !empty($parts['port']) ? ':'. $parts['port'] : '';
    $out .= "/". ltrim($parts['path'], '/');
    return $out;
  }

  /**
   * Get the form for the settings for this destination.
   */
  function _xmlrpc() {
    $args = func_get_args();
    $url = $this->url(FALSE) . '/services/xmlrpc';
    array_unshift($args, $url);

    $out = call_user_func_array('xmlrpc', $args);

    // Check for errors.
    $err = xmlrpc_error();
    if ($err && $err->is_error) {
      _backup_migrate_message('The server returned the following error: %err', array('%err' => $err->message), 'error');
      return FALSE;
    }
    return $out;
  }
}
