
-------------------------------------------------------------------------------
Backup and Migrate Client for Drupal 6.x
  by Ronan Dowling, Gorton Studios - ronan (at) gortonstudios (dot) com
-------------------------------------------------------------------------------

DESCRIPTION:
This module adds 'Backup and Migrate Server' support to Backup and Migrate.  

For installation instructions see the README.txt for Backup and Migrate Server.