
-------------------------------------------------------------------------------
Backup and Migrate Server for Drupal 6.x
  by Ronan Dowling, Gorton Studios - ronan (at) gortonstudios (dot) com
-------------------------------------------------------------------------------

DESCRIPTION:
This module allows you to create a server to be a remote destination for
your backup and migrate backups. 

-------------------------------------------------------------------------------

REQUIREMENTS (SERVER):
* Backup and Migrate  http://drupal.org/project/backup_migrate
* Secure Sites        http://drupal.org/project/securesite

INSTALLATION (SERVER):
1. Install and configure Backup and Migrate 
2. Install Secure Sites and configure to use 'HTTP Basic' and set 
    Force Authentication to 'Always' or 'On restricted pages'
3. Install and enable the 'Backup and Migrate Server' module.
4. Create one or more Backup and Migrate destinations and check the 
    'Allow remote access' checkbox.
5. Create one or more users with the 'use backup server api' permission.

REQUIREMENTS (CLIENT):
* Backup and Migrate  http://drupal.org/project/backup_migrate

INSTALLATION (CLIENT):
1. Install and configure Backup and Migrate 
2. Install and enable the 'Backup and Migrate Client' module (included with this
     module).
3. Create a new destination. Pick 'Backup and Migrate Server' as the type.
4. Enter the username and password for one of the users created in step 5 above
    then click 'Next Step'
5. Select one on of the server destinations created in step 4 above from the
pulldown. Save the destination.

You will now be able to backup to the server destination on your client site and
it will be saved to the destination on the server.
