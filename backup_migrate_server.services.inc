<?php
/**
 * @file backup_migrate_server.service.inc
 * service.module hooks for the Backup and Migrate server.
 */


function backup_migrate_server_service_access() {
  return user_access('use backup server api');
}
function backup_migrate_server_service_access_write($destination_id) {
  if (user_access('use backup server api')) {
    if ($destination = backup_migrate_server_get_destination($destination_id)) {
      return backup_migrate_server_destination_access($destination, 'write');
    }
    // Return true here to allow more useful error checking take over.
    return TRUE;
  }
  return FALSE;
}
function backup_migrate_server_service_access_read($destination_id) {
  if (user_access('use backup server api')) {
    if ($destination = backup_migrate_server_get_destination($destination_id)) {
      return backup_migrate_server_destination_access($destination, 'read');
    }
    // Return true here to allow more useful error checking take over.
    return TRUE;
  }
  return FALSE;
}


/**
 * List all the user's destinations.
 */
function backup_migrate_server_service_list_destinations() {
  // Set the message mode to server.
  _backup_migrate_message_callback('_backup_migrate_message_server');

  $out = array();

  foreach (backup_migrate_server_get_destinations() as $dest) {
    $out[] = array(
      'id'    => $dest->get('id'),
      'name'  => $dest->name
    );
  }
  return $out;
}

/**
 * List all of the files in a destination.
 */
function backup_migrate_server_service_list_files($destination_id = NULL) {
  // Set the message mode to server.
  _backup_migrate_message_callback('_backup_migrate_message_server');

  if ($destination = backup_migrate_server_get_destination($destination_id)) {
    $out = array();
    foreach ($destination->list_files() as $file) {
      if ($file->is_recognized_type()) {
        $out[] = array(
          'filename' => $file->info('filename'),
          'filesize' => $file->info('filesize'),
          'filetime' => $file->info('filetime'),
        );
      }
    }
  }
  return $out;
}

/**
 * Delete the file in the destination.
 */
function backup_migrate_server_service_delete_file($destination_id, $file_id) {
  // Set the message mode to server.
  _backup_migrate_message_callback('_backup_migrate_message_server');

  if ($destination = backup_migrate_server_get_destination($destination_id)) {
    $destination->delete_file($file_id);
  }
  return backup_migrate_server_error();
}


/**
 * Get a download ticket.
 */
function backup_migrate_server_service_get_download_ticket($destination_id, $file_id) {
  // Set the message mode to server.
  _backup_migrate_message_callback('_backup_migrate_message_server');

  $destination = backup_migrate_server_get_destination($destination_id);
  if ($destination && $destination->file_exists($file_id)) {
    $destination_id = $destination->get_id();
    $url = url('backups/download/'. $destination_id .'/'. $file_id, array('absolute' => TRUE));
    $out = array('url' => $url, 'auth' => 'basic');
  }
  drupal_alter('backup_migrate_server_download_ticket', $out, $destination, $file_id);
  if ($out) {
    return $out;
  }
  return backup_migrate_server_error();
}

/**
 * Get a upload ticket.
 */
function backup_migrate_server_service_get_upload_ticket($destination_id, $file_id, $file_size) {
  // Set the message mode to server.
  _backup_migrate_message_callback('_backup_migrate_message_server');

  $out = array();

  $destination = backup_migrate_server_get_destination($destination_id);
  if ($destination) {
    $url = url('backups/upload/'. $destination_id, array('absolute' => TRUE));
    $out = array('url' => $url, 'auth' => 'basic', 'params' => array());
  }
  drupal_alter('backup_migrate_server_upload_ticket', $out, $destination, $file_id, $file_size);
  if ($out) {
    return $out;
  }
  return backup_migrate_server_error();
}

/**
 * Get a upload ticket.
 */
function backup_migrate_server_service_confirm_upload($destination_id, $file_id, $file_size) {
  // Set the message mode to server.
  _backup_migrate_message_callback('_backup_migrate_message_server');

  if ($destination = backup_migrate_server_get_destination($destination_id)) {
    $files = $destination->list_files();
    if (!isset($files[$file_id])) {
      _backup_migrate_message('The backup file was not uploaded successfuly.', array(), 'error');
      return backup_migrate_server_error();
    }
    else if ($files[$file_id]->info('filesize') != $file_size) {
      _backup_migrate_message('The uploaded file was !size bytes it should have been !expected bytes.', array('!size' => $files[$file_id]->info('filesize'), '!expected' => $file_size), 'error');
      return backup_migrate_server_error();
    }
  }
  $out = array('success' => TRUE);
  drupal_alter('backup_migrate_server_upload_confirmation', $out);
  return $out;
}

